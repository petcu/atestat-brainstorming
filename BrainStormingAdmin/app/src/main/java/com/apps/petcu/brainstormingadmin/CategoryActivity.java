package com.apps.petcu.brainstormingadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.apps.petcu.brainstormingadmin.bottom_sheet.BottomSheetCategory;
import com.apps.petcu.brainstormingadmin.databinding.ActivityCategoryBinding;
import com.apps.petcu.brainstormingadmin.model.Category;
import com.apps.petcu.brainstormingadmin.model.Question;
import com.apps.petcu.brainstormingadmin.utilities.Tools;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class CategoryActivity extends AppCompatActivity {
    private static final int PICK_IMAGE_REQUEST = 1;
    private ActivityCategoryBinding mBinding;
    private StorageReference mStorageRef;
    private DatabaseReference mDatabaseRef;
    private StorageTask mUploadTask;
    private Uri mIconUri = null;
    private Category mCategory = null;
    private boolean isFromIntent, iconSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_category);
        setValuesFromIntent();
        getFirebaseReferences();
        setHintTextEdit();
        mBinding.categoryImageView.setOnClickListener( (View v) -> selectIconImage() );
        mBinding.categoryButtonUpload.setOnClickListener( (View v) -> uploadCategory() );
    }

    //Get Category from Intent
    private void setValuesFromIntent(){
        mCategory = getIntent().getParcelableExtra(BottomSheetCategory.CATEGORY_MODIFY);
        isFromIntent = getIntent().getBooleanExtra(BottomSheetCategory.CATEGORY_EDIT_MODE, false);
        if(isFromIntent){
            mBinding.setCategory(mCategory);
            Picasso.get().load(mCategory.getIconUrl()).placeholder(R.drawable.image_holder).into(mBinding.categoryImageView);
        }else
            mCategory = new Category();
    }

    //Get Database and Storage Reference
    private void getFirebaseReferences(){
        String pathDatabase = getString(R.string.categories_database_root);
        String pathStorage = getString(R.string.icons_storage_root);
        mStorageRef = FirebaseStorage.getInstance().getReference(pathStorage);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(pathDatabase);
    }

    //Start intent to select a category report
    private void selectIconImage(){
        Intent intent = new Intent().setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    //Verify Categoty Name and Icon Link
    private boolean validateInput(){
        String category_name = mBinding.categoryEdit.getText().toString().trim();
        if(category_name.equals("")) {
            Toast.makeText(this, "Category need a name.", Toast.LENGTH_SHORT).show();
            return false;
        }else if(mIconUri == null && !isFromIntent){
            Toast.makeText(this, "Icon is not selected.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    //Upload / Update Category
    private void uploadCategory(){
        if(mUploadTask != null && mUploadTask.isInProgress())
            Toast.makeText(this,"Upload in progress.", Toast.LENGTH_SHORT).show();
        else if(validateInput()){
            if(isFromIntent){
                String old_category = mCategory.getName();
                String new_category = mBinding.categoryEdit.getText().toString().trim();

                if(mIconUri != null && iconSelect) {
                    deleteIcon();
                    createNewCategory();
                }

                if(!old_category.equals(new_category)) {
                    Handler handler = new Handler();
                    handler.postDelayed(() -> changeName(), 500);
                }
            }else
                createNewCategory();
        }
    }

    //Upload Database Categories
    private void updateDatabase(){
        String category_name = mBinding.categoryEdit.getText().toString().trim();
        String uploadID = (isFromIntent ? mCategory.getKey() : mDatabaseRef.push().getKey() );
        mCategory.setName(category_name);
        mDatabaseRef.child(Objects.requireNonNull(uploadID)).setValue(mCategory);
        iconSelect = false;
        Toast.makeText(this, "Category Created", Toast.LENGTH_SHORT).show();
    }

    //If we change Category Icon
    private void deleteIcon(){
        StorageReference imageRef = FirebaseStorage.getInstance().getReferenceFromUrl(mCategory.getIconUrl());
        imageRef.delete().addOnFailureListener( (@NonNull Exception e) ->
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show()
        );
    }

    //Get Icon Reference and Upload In Storage
    private void createNewCategory(){
        StorageReference fileReference = mStorageRef.child(Tools.getUUID(this, mIconUri));
        mUploadTask = fileReference.putFile(mIconUri).addOnSuccessListener((UploadTask.TaskSnapshot task) -> {
            handleProgressBar();
            fileReference.getDownloadUrl().addOnSuccessListener((Uri uri) -> {
                mCategory.setIconUrl(uri.toString());
                updateDatabase();
            });
        }).addOnFailureListener((@NonNull Exception e) ->
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show()
        ).addOnProgressListener((UploadTask.TaskSnapshot task) -> {
            double progress = (100.0 * task.getBytesTransferred() / task.getTotalByteCount());
            mBinding.categoryProgressBar.setProgress((int) progress);
        });

    }

    //Change Category Name
    private void changeName(){
        String path_categories = getString(R.string.categories_database_root);
        String path_questions = getString(R.string.questions_database_root);
        String old_name = mCategory.getName();
        String new_name = mBinding.categoryEdit.getText().toString().trim();
        String new_category = path_questions + "/" + new_name;
        String old_category = path_questions + "/" + old_name;

        moveQuestions(old_category, new_category);
        mCategory.setName(new_name);

        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child(path_categories);
        db.child(mCategory.getKey()).setValue(mCategory);
    }

    //Mode Questions From Old Category to New Category
    private void moveQuestions(String old_category, String new_category){
        DatabaseReference db_old_category = FirebaseDatabase.getInstance().getReference().child(old_category);
        DatabaseReference db_new_category = FirebaseDatabase.getInstance().getReference().child(new_category);
        db_old_category.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot){
                for(DataSnapshot data : dataSnapshot.getChildren()){
                    Question question = data.getValue(Question.class);
                    String key = data.getKey();
                    db_new_category.child(Objects.requireNonNull(key)).setValue(question);
                }

                //Remove Old Category
                db_old_category.removeValue().addOnSuccessListener( (Void aVoid) ->
                        Toast.makeText(CategoryActivity.this, "Category Updated", Toast.LENGTH_SHORT).show()
                ).addOnFailureListener( (@NonNull Exception e) ->
                        Toast.makeText(CategoryActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show()
                );
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(CategoryActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Delay ProgressBar final status
    private void handleProgressBar(){
        Handler handler = new Handler();
        handler.postDelayed(() -> mBinding.categoryProgressBar.setProgress(0), 500);
    }

    //Get Image Path
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            mIconUri = data.getData();
            iconSelect = true;
            Picasso.get().load(mIconUri).into(mBinding.categoryImageView);
        }
    }

    //Set EditText Hint
    private void setHintTextEdit() {
        mBinding.categoryEdit.setOnFocusChangeListener((v, hasFocus) ->
                mBinding.categoryEdit.setHint(hasFocus ? "" : getString(R.string.CategoryEdit))
        );
    }
}
