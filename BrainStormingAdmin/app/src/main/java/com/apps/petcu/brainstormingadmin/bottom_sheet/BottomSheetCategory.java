package com.apps.petcu.brainstormingadmin.bottom_sheet;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.apps.petcu.brainstormingadmin.CategoryActivity;
import com.apps.petcu.brainstormingadmin.QuestionActivity;
import com.apps.petcu.brainstormingadmin.QuestionListActivity;
import com.apps.petcu.brainstormingadmin.R;
import com.apps.petcu.brainstormingadmin.model.Category;
import com.apps.petcu.brainstormingadmin.model.Question;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import androidx.annotation.NonNull;

public class BottomSheetCategory extends BottomSheetDialogFragment {
    //Question TAG
    public static final String QUESTION_SELECT_TAG = "QUESTION_SELECT_TAG";
    public static final String NEW_QUESTION_TAG = "NEW_QUESTION_TAG";
    public static final String QUESTIONS_LIST_TAG = "QUESTIONS_LIST_TAG";
    //Category TAG
    public static final String CATEGORY_MODIFY = "CATEGORY_MODIFY";
    public static final String CATEGORY_EDIT_MODE = "CATEGORY_EDIT_MODE";

    private Context mContext;
    private Category mCategory;

    public BottomSheetCategory(){}

    @SuppressLint("ValidFragment")
    public BottomSheetCategory(Context context, Category category){
        mContext = context;
        mCategory = category;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = View.inflate(getContext(), R.layout.bottom_sheet,null);

        //Add OnClickListener for buttons
        view.findViewById(R.id.add_question).setOnClickListener( (View v) -> newQuestion() );
        view.findViewById(R.id.show_questions).setOnClickListener( (View v) -> showQuestionsList() );
        view.findViewById(R.id.modify_category).setOnClickListener( (View v) -> modifyCategory() );
        view.findViewById(R.id.delete_category).setOnClickListener( (View v) -> onDeleteCategory() );
        dialog.setContentView(view);
    }

    //Create a new Question
    private void newQuestion() {
        Intent intent = new Intent(mContext, QuestionActivity.class);
        Question q = new Question();
        q.setCategory(mCategory.getName());
        intent.putExtra(QUESTION_SELECT_TAG, q);
        intent.putExtra(NEW_QUESTION_TAG,true);
        startActivity(intent);
    }

    //Show Questions for a Category
    private void showQuestionsList(){
        Intent intent = new Intent(mContext, QuestionListActivity.class);
        intent.putExtra(QUESTIONS_LIST_TAG, mCategory.getName());
        startActivity(intent);
    }

    //Modify a Category
    private void modifyCategory(){
        Intent intent = new Intent(mContext, CategoryActivity.class);
        intent.putExtra(CATEGORY_MODIFY, mCategory);
        intent.putExtra(CATEGORY_EDIT_MODE, true);
        startActivity(intent);
    }

    //Delete a Category
    private void onDeleteCategory() {
        deleteQuestions();
        String selectedKey = mCategory.getKey();
        String path = getString(R.string.categories_database_root);
        String iconURL = mCategory.getIconUrl();

        DatabaseReference db = FirebaseDatabase.getInstance().getReference(path);
        StorageReference imageRef = FirebaseStorage.getInstance().getReferenceFromUrl(iconURL);

        imageRef.delete().addOnSuccessListener( (Void v) -> {
            db.child(selectedKey).removeValue();
            Toast.makeText(mContext, "Category deleted", Toast.LENGTH_SHORT).show();
        });
    }

    private void deleteQuestions(){
        String path = getString(R.string.questions_database_root);
        String category_name = mCategory.getName();
        DatabaseReference db = FirebaseDatabase.getInstance().getReference(path);
        db.child(category_name).removeValue();
    }
}
