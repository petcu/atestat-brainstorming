package com.apps.petcu.brainstormingadmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.apps.petcu.brainstormingadmin.bottom_sheet.BottomSheetCategory;
import com.apps.petcu.brainstormingadmin.model.Question;
import com.apps.petcu.brainstormingadmin.databinding.ActivityQuestionBinding;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class QuestionActivity extends AppCompatActivity {
    private Question currentQuestion;
    private ActivityQuestionBinding mBinding;
    private String question, option1, option2, option3, option4, answer, category, key=null;
    private int radioSelect;
    private boolean newQuestion, isIntent=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_question);
        setValuesFromIntent();
        setHintTextEdit();
        setRadioGroup();

        mBinding.buttonQuestionSubmit.setOnClickListener( (View v) -> {
            getValueFromLayout();
            if(validateInput()){
                getCurrentQuestion();
                uploadQuestion();
            }
        });
    }

    //Get Question from Intent
    private void setValuesFromIntent(){
        currentQuestion = getIntent().getParcelableExtra(BottomSheetCategory.QUESTION_SELECT_TAG);
        newQuestion = getIntent().getBooleanExtra(BottomSheetCategory.NEW_QUESTION_TAG, true);
        if(currentQuestion != null) {
            isIntent = true;
            category = currentQuestion.getCategory();
            key = currentQuestion.getKey();
            mBinding.textviewCategoryName.setText(category);
            if(!newQuestion)
                mBinding.setQuestion(currentQuestion);
        }
    }

    //Get Values from Layout
    private void getValueFromLayout(){
        question = mBinding.questionText.getText().toString().trim();
        option1 = mBinding.questionOption1.getText().toString().trim();
        option2 = mBinding.questionOption2.getText().toString().trim();
        option3 = mBinding.questionOption3.getText().toString().trim();
        option4 = mBinding.questionOption4.getText().toString().trim();
        answer = getAnswer();
    }

    //Validate Input
    private boolean validateInput(){
        boolean valid = true;
        if(question.equals("") || option1.equals("") || option2.equals("") || option3.equals("") || option4.equals("")){
            Toast.makeText(this,"All fields must be filled", Toast.LENGTH_SHORT).show();
            valid = false;
        }else if(radioSelect == 0){
            Toast.makeText(this, "Select an answer.", Toast.LENGTH_SHORT).show();
            valid = false;
        }
        return valid;
    }

    //Get answer from RadioGroup
    private String getAnswer(){
        String select_answer = "";
        switch(radioSelect){
            case R.id.radio_option1:
                select_answer = option1;
                break;
            case R.id.radio_option2:
                select_answer = option2;
                break;
            case R.id.radio_option3:
                select_answer = option3;
                break;
            case R.id.radio_option4:
                select_answer = option4;
                break;
        }
        return select_answer;
    }

    //Set currentQuestion with data from layout
    private void getCurrentQuestion(){
        currentQuestion = new Question(question, option1, option2, option3, option4, answer);
        currentQuestion.setKey(key);
        currentQuestion.setCategory(category);
    }

    //Upload new Question
    private void uploadQuestion(){
        String path = getString(R.string.questions_database_root);
        DatabaseReference db = FirebaseDatabase.getInstance().getReference(path).child(category);
        key = (newQuestion ? db.push().getKey() : key);
        db.child(Objects.requireNonNull(key)).setValue(currentQuestion);
        Toast.makeText(this, isIntent ? "Question updated": "Question uploaded",Toast.LENGTH_SHORT).show();
    }

    //Set Hint for EditText
    private void setHintTextEdit(){
        mBinding.questionText.setOnFocusChangeListener((v, hasFocus) ->
                mBinding.questionText.setHint(hasFocus ? "":getString(R.string.HintQuestion))
        );
        mBinding.questionOption1.setOnFocusChangeListener((v, hasFocus) ->
                mBinding.questionOption1.setHint(hasFocus ? "":getString(R.string.HintOption))
        );
        mBinding.questionOption2.setOnFocusChangeListener((v, hasFocus) ->
                mBinding.questionOption2.setHint(hasFocus ? "":getString(R.string.HintOption))
        );
        mBinding.questionOption3.setOnFocusChangeListener((v, hasFocus) ->
                mBinding.questionOption3.setHint(hasFocus ? "":getString(R.string.HintOption))
        );
        mBinding.questionOption4.setOnFocusChangeListener((v, hasFocus) ->
                mBinding.questionOption4.setHint(hasFocus ? "":getString(R.string.HintOption))
        );
    }

    //Set Radio Group
    private void setRadioGroup(){
        mBinding.radioGroup1.setOnCheckedChangeListener( (RadioGroup group, int checkedId) -> {
            if(checkedId == R.id.radio_option1 || checkedId == R.id.radio_option2) {
                mBinding.radioGroup2.clearCheck();
                group.check(checkedId);
                radioSelect = checkedId;
            }
        });
        mBinding.radioGroup2.setOnCheckedChangeListener( (RadioGroup group, int checkedId) -> {
            if(checkedId == R.id.radio_option3 || checkedId == R.id.radio_option4) {
                mBinding.radioGroup1.clearCheck();
                group.check(checkedId);
                radioSelect = checkedId;
            }
        });
    }
}
