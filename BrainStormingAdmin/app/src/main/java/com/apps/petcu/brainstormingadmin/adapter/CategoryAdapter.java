package com.apps.petcu.brainstormingadmin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.petcu.brainstormingadmin.R;
import com.apps.petcu.brainstormingadmin.model.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ImageViewHolder> {
    private Context mContext;
    private List<Category> mCategories;
    private OnAdapterItemClickListener mListener;

    public CategoryAdapter(Context context, List<Category> categories) {
        mContext = context;
        mCategories = categories;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.category_item, parent, false);
        return new ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        Category categoryCurrent = mCategories.get(position);
        holder.textViewName.setText(categoryCurrent.getName());
        Picasso.get()
                .load(categoryCurrent.getIconUrl())
                .placeholder(R.mipmap.ic_launcher)
                .fit()
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    //Listener
    public void setOnItemClickListener(OnAdapterItemClickListener listener) {
        mListener = listener;
    }

    //Interface for OnClickListener for BottomSheetCategory
    public interface OnAdapterItemClickListener {
        void setBottomSheet(int position);
    }

    // Holder Class that take care of a Category Item
    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView textViewName;
        private ImageView imageView;

        public ImageViewHolder(View itemView) {
            super(itemView);

            //Get Layout References
            textViewName = itemView.findViewById(R.id.text_view_name);
            imageView = itemView.findViewById(R.id.image_view_upload);

            //Set View.OnClickListener for Question Item
            itemView.setOnClickListener(this);
        }

        //Set interface method "setBottomSheet" for the Category Item
        @Override
        public void onClick(View v) {
            if(mListener != null){
                int position = getAdapterPosition();
                if(position != RecyclerView.NO_POSITION)
                    mListener.setBottomSheet(position);
            }
        }
    }
}