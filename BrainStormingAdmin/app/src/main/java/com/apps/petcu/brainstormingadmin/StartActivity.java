package com.apps.petcu.brainstormingadmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apps.petcu.brainstormingadmin.databinding.ActivityStartBinding;

public class StartActivity extends AppCompatActivity {
    private ActivityStartBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_start);
        mBinding.startAddCategory.setOnClickListener( (View v) -> addCategory() );
        mBinding.startShowCategories.setOnClickListener( (View v) -> showCategories() );
    }

    //Start CategoryActivity
    private void addCategory(){
        Intent intent = new Intent(this, CategoryActivity.class);
        startActivity(intent);
    }

    //Start CategoryListActivity
    private void showCategories(){
        Intent intent = new Intent(this, CategoryListActivity.class);
        startActivity(intent);
    }
}
