package com.apps.petcu.brainstormingadmin.utilities;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.apps.petcu.brainstormingadmin.model.Question;

public class Tools {

    public static String getFileExtension(Context context, Uri uri) {
        ContentResolver cR = context.getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    public static String getUUID(Context context, Uri uri){
        String ID = System.currentTimeMillis() + "." + getFileExtension(context, uri);
        return ID;
    }

    public static void makeToast(Context context, String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    public static boolean haveAnAnswer(Question question){
        String answer = question.getAnswer();
        String option1 = question.getOption1();
        String option2 = question.getOption2();
        String option3 = question.getOption3();
        String option4 = question.getOption4();
        return (answer.equals(option1) || answer.equals(option2) || answer.equals(option3) || answer.equals(option4));
    }

}
