package com.apps.petcu.brainstormingadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.apps.petcu.brainstormingadmin.adapter.QuestionAdapter;
import com.apps.petcu.brainstormingadmin.bottom_sheet.BottomSheetCategory;
import com.apps.petcu.brainstormingadmin.bottom_sheet.BottomSheetQuestion;
import com.apps.petcu.brainstormingadmin.databinding.ActivityQuestionListBinding;
import com.apps.petcu.brainstormingadmin.model.Question;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class QuestionListActivity extends AppCompatActivity implements QuestionAdapter.OnAdapterItemClickListener{
    private ActivityQuestionListBinding mBinding;
    private QuestionAdapter mAdapter;
    private DatabaseReference mDatabaseRef;
    private ValueEventListener mDBListener;
    private List<Question> mQuestions;
    private String mCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_question_list);

        assignValues();
        setRecyclerView();

        //Generate Questions List and Send to Adapter
        mDBListener = mDatabaseRef.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mQuestions.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Question question = postSnapshot.getValue(Question.class);
                    Objects.requireNonNull(question).setCategory(mCategory);
                    question.setKey(postSnapshot.getKey());
                    mQuestions.add(question);
                }
                mAdapter.notifyDataSetChanged();
                mBinding.progressCircleQlist.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(QuestionListActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                mBinding.progressCircleQlist.setVisibility(View.INVISIBLE);
            }
        });
    }

    //Get References
    private void assignValues(){
        mQuestions = new ArrayList<>();
        mCategory = getIntent().getStringExtra(BottomSheetCategory.QUESTIONS_LIST_TAG);
        mBinding.categoryNameQlist.setText(mCategory);

        String path = getString(R.string.questions_database_root) + "/" + mCategory;
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(path);
    }

    //Set RecyclerView and Adapter
    private void setRecyclerView(){
        mAdapter = new QuestionAdapter(QuestionListActivity.this, mQuestions);
        mAdapter.setOnItemClickListener(QuestionListActivity.this);
        mBinding.recyclerViewQlist.setAdapter(mAdapter);
        mBinding.recyclerViewQlist.setHasFixedSize(true);
        mBinding.recyclerViewQlist.setLayoutManager(new LinearLayoutManager(this));
    }

    //From Interface QuestionAdapter.OnAdapterItemClickListener
    @Override
    public void setBottomSheet(int position){
        BottomSheetDialogFragment bottomSheetDialogFragment = new BottomSheetQuestion(this, mQuestions.get(position));
        bottomSheetDialogFragment.setShowsDialog(true);
        bottomSheetDialogFragment.show(getSupportFragmentManager(),bottomSheetDialogFragment.getTag());
    }

    //Remove Database Listener from memory when close the activity
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDatabaseRef.removeEventListener(mDBListener);
    }
}
