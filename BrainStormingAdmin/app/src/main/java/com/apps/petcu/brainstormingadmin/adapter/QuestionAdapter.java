package com.apps.petcu.brainstormingadmin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.petcu.brainstormingadmin.R;
import com.apps.petcu.brainstormingadmin.model.Question;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder> {
    private Context mContext;
    private List<Question> mQuestions;
    private OnAdapterItemClickListener mListener;

    public QuestionAdapter(Context context, List<Question> questions){
        mContext = context;
        mQuestions = questions;
    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.question_item, parent, false);
        return new QuestionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder holder, int position) {
        Question question = mQuestions.get(position);
        holder.questionText.setText(question.getQuestion());
        holder.questionAnswer.setText(question.getAnswer());
    }

    @Override
    public int getItemCount() {
        return mQuestions.size();
    }

    //Listener
    public void setOnItemClickListener(OnAdapterItemClickListener listener) {
        mListener = listener;
    }

    //Interface for OnClickListener for BottomSheetCategory
    public interface OnAdapterItemClickListener {
        void setBottomSheet(int position);
    }

    // Holder Class that take care of a Question Item
    public class QuestionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView questionText, questionAnswer;

        public QuestionViewHolder(@NonNull View itemView) {
            super(itemView);

            //Get TextView References
            questionText = itemView.findViewById(R.id.question_item_text);
            questionAnswer = itemView.findViewById(R.id.question_item_answer);

            //Set View.OnClickListener for Question Item
            itemView.setOnClickListener(this);
        }

        //Set interface method "setBottomSheet" for the Question Item
        @Override
        public void onClick(View v){
            if(mListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION)
                    mListener.setBottomSheet(position);
            }
        }
    }
}
