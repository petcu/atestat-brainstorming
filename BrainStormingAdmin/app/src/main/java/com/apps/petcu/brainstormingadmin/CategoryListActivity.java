package com.apps.petcu.brainstormingadmin;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.apps.petcu.brainstormingadmin.adapter.CategoryAdapter;
import com.apps.petcu.brainstormingadmin.bottom_sheet.BottomSheetCategory;
import com.apps.petcu.brainstormingadmin.databinding.ActivityCategoryListBinding;
import com.apps.petcu.brainstormingadmin.model.Category;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

public class CategoryListActivity extends AppCompatActivity implements CategoryAdapter.OnAdapterItemClickListener {
    private ActivityCategoryListBinding mBinding;
    private CategoryAdapter mAdapter;
    private DatabaseReference mDatabaseRef;
    private ValueEventListener mDBListener;
    private List<Category> mCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_category_list);

        assignValues();
        setRecyclerView();

        mDBListener = mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mCategories.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Category category = postSnapshot.getValue(Category.class);
                    category.setKey(postSnapshot.getKey());
                    mCategories.add(category);
                }

                mAdapter.notifyDataSetChanged();
                mBinding.progressCircle.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(CategoryListActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                mBinding.progressCircle.setVisibility(View.INVISIBLE);
            }
        });
    }

    //Get References
    private void assignValues(){
        mCategories = new ArrayList<>();
        String path = getString(R.string.categories_database_root);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(path);
    }

    //Set RecyclerView and Adapter
    private void setRecyclerView(){
        mAdapter = new CategoryAdapter(CategoryListActivity.this, mCategories);
        mAdapter.setOnItemClickListener(CategoryListActivity.this);
        mBinding.recyclerView.setAdapter(mAdapter);
        mBinding.recyclerView.setHasFixedSize(true);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    //From Interface QuestionAdapter.OnAdapterItemClickListener
    @Override
    public void setBottomSheet(int position){
        BottomSheetDialogFragment bottomSheetDialogFragment = new BottomSheetCategory(this, mCategories.get(position));
        bottomSheetDialogFragment.setShowsDialog(true);
        bottomSheetDialogFragment.show(getSupportFragmentManager(),bottomSheetDialogFragment.getTag());
    }

    //Remove Database Listener from memory when close the activity
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDatabaseRef.removeEventListener(mDBListener);
    }
}