package com.apps.petcu.brainstormingadmin.bottom_sheet;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.apps.petcu.brainstormingadmin.QuestionActivity;
import com.apps.petcu.brainstormingadmin.R;
import com.apps.petcu.brainstormingadmin.model.Question;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;

public class BottomSheetQuestion extends BottomSheetDialogFragment {
    public static final String QUESTION_SELECT_TAG = "QUESTION_SELECT_TAG";
    public static final String NEW_QUESTION_TAG = "NEW_QUESTION_TAG";
    private Context mContext;
    private Question mQuestion;

    public BottomSheetQuestion(){}

    @SuppressLint("ValidFragment")
    public BottomSheetQuestion(Context context, Question question){
        mContext = context;
        mQuestion = question;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = View.inflate(getContext(), R.layout.bottom_sheet_question,null);
        view.findViewById(R.id.modify_question).setOnClickListener( (View v) -> modifyQuestion() );
        view.findViewById(R.id.delete_question).setOnClickListener( (View v) -> deleteQuestion() );
        dialog.setContentView(view);
    }

    private void modifyQuestion(){
        Intent intent = new Intent(mContext, QuestionActivity.class);
        intent.putExtra(QUESTION_SELECT_TAG, mQuestion);
        intent.putExtra(NEW_QUESTION_TAG,false);
        startActivity(intent);
    }

    private void deleteQuestion(){
        final String selectedKey = mQuestion.getKey();
        final String path = getString(R.string.questions_database_root) + "/" + mQuestion.getCategory();
        DatabaseReference mDatabaseRef = FirebaseDatabase.getInstance().getReference(path);
        mDatabaseRef.child(selectedKey).removeValue().addOnSuccessListener( (Void aVoid) ->
            Toast.makeText(mContext, "Question deleted.", Toast.LENGTH_SHORT).show()
        );
    }
}
