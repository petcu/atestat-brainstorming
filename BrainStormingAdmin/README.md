## BrainStormingAdmin
<p align="center">
  <img src="https://i.imgur.com/0lGKTCV.png" alt="BrainStormingAdmin"/>
</p>

### Download
https://github.com/PMS25/BrainStormingAdmin/releases

### Schreenshots
<p align="center">
  <img src="https://i.imgur.com/kOnoNlt.png" alt="screenshot_1"/>
  <img src="https://i.imgur.com/xJ3StKU.png" alt="screenshot_2"/>
  <img src="https://i.imgur.com/5QYAs7f.png" alt="screenshot_3"/>
  <img src="https://i.imgur.com/kz0cWZE.png" alt="screenshot_4"/>
  <img src="https://i.imgur.com/UxsxY4n.png" alt="screenshot_5"/>
</p>
