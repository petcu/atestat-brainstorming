package com.apps.petcu.brainstormingquiz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import com.apps.petcu.brainstormingquiz.databinding.ActivityStartBinding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class StartActivity extends AppCompatActivity {
    public static final String SHARED_PREFS = "SHARED_PREFS";
    public static final String SESSION = "SESSION";
    private ActivityStartBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_start);
        mBinding.startGame.setOnClickListener((View v) -> selectDomains());
        setSpinnerSession();
        loadSettings();
    }

    //Open Domain Selection List
    private void selectDomains() {
        saveSettings();
        Intent intent = new Intent(this, DomainListActivity.class);
        startActivity(intent);
    }

    //Set Snipper Select Session
    private void setSpinnerSession() {
        ArrayAdapter<CharSequence> adapter;
        adapter = ArrayAdapter.createFromResource(this, R.array.session, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.settingsSession.setAdapter(adapter);
    }

    //Save Shared Preferences
    public void saveSettings() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        int item_selected = mBinding.settingsSession.getSelectedItemPosition();
        editor.putInt(SESSION, item_selected);
        editor.apply();
    }

    //Load Shared Preferences
    public void loadSettings() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        int selected = sharedPreferences.getInt(SESSION, 0);
        mBinding.settingsSession.setSelection(selected);
    }
}
