package com.apps.petcu.brainstormingquiz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.apps.petcu.brainstormingquiz.databinding.ActivityStatisticsBinding;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

public class StatisticsActivity extends AppCompatActivity {
    private ActivityStatisticsBinding mBinding;
    private int correct, wrong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_statistics);
        mBinding.restartButton.setOnClickListener( (View v) -> restartGame() );
        assignValue();
        loadPieChart();
    }

    //Assign Values
    private void assignValue(){
        correct = getIntent().getIntExtra(QuizActivity.CORRECT_QUESTIONS,0);
        //Get length session
        SharedPreferences sharedPreferences = getSharedPreferences(StartActivity.SHARED_PREFS, MODE_PRIVATE);
        String[] options = getResources().getStringArray(R.array.session);
        int select = sharedPreferences.getInt(StartActivity.SESSION,0);
        int length = Integer.valueOf(options[select]);
        wrong = length - correct;
    }

    //Load PieChart
    private void loadPieChart(){
        //Get DataSet
        PieDataSet pieDataSet = new PieDataSet(getData(),"");
        pieDataSet.setColors(getColors());

        //Get Data from DataSet
        PieData pieData = new PieData(pieDataSet);
        pieData.setValueTextColor(getResources().getColor(R.color.data_text));
        pieData.setValueTextSize(20);

        //Set PieChart Atributes
        mBinding.pieChart.setCenterText("Answers");
        mBinding.pieChart.setCenterTextColor(getResources().getColor(R.color.green_1));
        mBinding.pieChart.setCenterTextSize(20);
        mBinding.pieChart.setDrawEntryLabels(true);
        mBinding.pieChart.setUsePercentValues(true);
        mBinding.pieChart.setRotationEnabled(false);

        //Disable Description and Legend
        mBinding.pieChart.getDescription().setEnabled(false);
        mBinding.pieChart.getLegend().setEnabled(false);

        //Upload Data
        mBinding.pieChart.setData(pieData);
        mBinding.pieChart.invalidate();
    }

    //Get Data gor PieChart
    private ArrayList<PieEntry> getData(){
        ArrayList<PieEntry> data = new ArrayList<>();
        data.add(new PieEntry(wrong, "Wrong"));
        data.add(new PieEntry(correct, "Correct"));
        return data;
    }

    //Get Colors for PieChart
    private int[] getColors(){
        return new int[]{getResources().getColor(R.color.wrong), getResources().getColor(R.color.correct)};
    }

    //Open Domain Select Activity
    private void restartGame(){
        //Intent intent = new Intent(this, DomainListActivity.class);
        Intent intent = new Intent(this, StartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
