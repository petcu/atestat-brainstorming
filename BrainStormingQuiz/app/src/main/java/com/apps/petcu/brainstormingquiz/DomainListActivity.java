package com.apps.petcu.brainstormingquiz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.apps.petcu.brainstormingquiz.adapter.DomainAdapter;
import com.apps.petcu.brainstormingquiz.databinding.ActivityDomainListBinding;
import com.apps.petcu.brainstormingquiz.model.Category;
import com.apps.petcu.brainstormingquiz.model.Question;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

public class DomainListActivity extends AppCompatActivity implements DomainAdapter.OnAdapterItemClickListener {
    private ActivityDomainListBinding mBinding;
    private DomainAdapter mAdapter;
    private DatabaseReference mDatabaseRef;
    private ValueEventListener mDBListener;
    private ArrayList<Category> mCategories;
    private ArrayList<String> mCategorySelected;
    private ArrayList<Question> mQuestions;

    public static final String QUIZ_QUESTIONS_TAG = "QUIZ_QUESTIONS_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_domain_list);
        asignValues();
        setRecyclerView();
        getCategories();
        mBinding.domainFab.setOnClickListener((View v) -> getQuestions());
    }

    //Get Categories List
    private void getCategories(){
        mDBListener = mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot){
                mCategories.clear();
                for(DataSnapshot data : dataSnapshot.getChildren()){
                    Category category = data.getValue(Category.class);
                    Objects.requireNonNull(category).setKey(data.getKey());
                    mCategories.add(category);
                }
                mAdapter.notifyDataSetChanged();
                mBinding.domainProgressCicle.setVisibility(View.INVISIBLE);
                mBinding.domainWorning.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(DomainListActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                mBinding.domainProgressCicle.setVisibility(View.INVISIBLE);
                mBinding.domainWorning.setVisibility(View.INVISIBLE);
            }
        });
    }

    //Get Questions List from CategorySelected
    private void getQuestions(){
        String path_questions = getString(R.string.questions_database_root);
        DatabaseReference db_questions = FirebaseDatabase.getInstance().getReference(path_questions);
        mQuestions.clear();

        if(startGameValid()) {
            db_questions.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot category : dataSnapshot.getChildren())
                        if (mCategorySelected.contains(category.getKey()))
                            for (DataSnapshot data : category.getChildren()) {
                                Question question = data.getValue(Question.class);
                                Objects.requireNonNull(question).setCategory(category.getKey());
                                question.setKey(data.getKey());
                                mQuestions.add(question);
                            }
                    startQuizActivity();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(DomainListActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else
            setMessage();
    }

    //Start Game
    private void startQuizActivity(){
        Intent intent = new Intent(this, QuizActivity.class);
        intent.putExtra(QUIZ_QUESTIONS_TAG, mQuestions);
        startActivity(intent);
    }

    //Assign Database and Values
    private void asignValues(){
        mCategorySelected = new ArrayList<>();
        mCategories = new ArrayList<>();
        mQuestions = new ArrayList<>();
        String path_category = getString(R.string.categories_database_root);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(path_category);
    }

    //Set RecyclerView
    private void setRecyclerView(){
        mAdapter = new DomainAdapter(DomainListActivity.this, mCategories);
        mAdapter.setOnDomainClickListener(DomainListActivity.this);
        mBinding.domainRecyclerview.setLayoutManager(new GridLayoutManager(DomainListActivity.this,3));
        mBinding.domainRecyclerview.setHasFixedSize(true);
        mBinding.domainRecyclerview.setAdapter(mAdapter);
    }

    private boolean startGameValid(){
        return mCategorySelected.size() >= 3;
    }

    private void setMessage(){
        if(mCategorySelected.size() == 0)
            mBinding.domainMessage.setText(R.string.domain_mesaj_1);
        else if(mCategorySelected.size() < 3)
            mBinding.domainMessage.setText(String.format(getString(R.string.domain_mesaj_2), 3-mCategorySelected.size()));
        else
            mBinding.domainMessage.setText(R.string.domain_mesaj_3);
    }

    //Add Category in Selected List
    @Override
    public void setDomainClick(CardView view, String category_name) {
        if(!mCategorySelected.contains(category_name)){
            mCategorySelected.add(category_name);
            view.setCardBackgroundColor(getResources().getColor(R.color.item_selected));
        }else{
            mCategorySelected.remove(category_name);
            view.setCardBackgroundColor(getResources().getColor(R.color.item_unselected));
        }
        setMessage();
    }

    //Remove Database Listener from memory when close the activity
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDatabaseRef.removeEventListener(mDBListener);
    }
}
