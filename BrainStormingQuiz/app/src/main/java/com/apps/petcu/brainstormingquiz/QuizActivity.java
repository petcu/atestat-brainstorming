package com.apps.petcu.brainstormingquiz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.apps.petcu.brainstormingquiz.databinding.ActivityQuizBinding;
import com.apps.petcu.brainstormingquiz.model.Question;
import com.apps.petcu.brainstormingquiz.tools.Tools;

import java.util.ArrayList;

public class QuizActivity extends AppCompatActivity {
    private ActivityQuizBinding mBinding;
    private ArrayList<Question> mQuestions;
    private Question currentQuestion;
    private int index = 1, session, correct=0;
    public static final String CORRECT_QUESTIONS = "CORRECT_QUESTIONS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_quiz);
        mQuestions = getIntent().getParcelableArrayListExtra(DomainListActivity.QUIZ_QUESTIONS_TAG);
        getSession();
        setLayout();
    }

    //Get Session Length
    private void getSession(){
        SharedPreferences sharedPreferences = getSharedPreferences(StartActivity.SHARED_PREFS, MODE_PRIVATE);
        int selected = sharedPreferences.getInt(StartActivity.SESSION,0);
        String[] aux = getResources().getStringArray(R.array.session);
        session = Integer.valueOf(aux[selected]);
    }

    //On Button Click
    public void onNext(View v){
        setButtonClick(false);
        Button button = (Button) v;
        Handler handler = new Handler();

        //Set Answer
        if(setTrueAnswer() != button)
            button.getBackground().setColorFilter(getResources().getColor(R.color.answer_false), PorterDuff.Mode.MULTIPLY);
        else
            correct++;

        //Load Next Question
        if(session > 1){
            index++;
            session--;
            handler.postDelayed( () -> setLayout() , 1000);
        }else
            handler.postDelayed( () -> openStatisticsActivity(), 1000);
    }

    //Set Buttons Clickable Atribute
    private void setButtonClick(boolean mode){
        mBinding.quizOption1.setClickable(mode);
        mBinding.quizOption2.setClickable(mode);
        mBinding.quizOption3.setClickable(mode);
        mBinding.quizOption4.setClickable(mode);
    }

    //Clear Buttons Background
    private void cleanAnswer(){
        mBinding.quizOption1.getBackground().clearColorFilter();
        mBinding.quizOption2.getBackground().clearColorFilter();
        mBinding.quizOption3.getBackground().clearColorFilter();
        mBinding.quizOption4.getBackground().clearColorFilter();
    }

    //Set True Answer
    private Button setTrueAnswer(){
        Button button = mBinding.quizOption1;
        String answer = currentQuestion.getAnswer();
        String option2 = mBinding.quizOption2.getText().toString();
        String option3 = mBinding.quizOption3.getText().toString();
        String option4 = mBinding.quizOption4.getText().toString();

        if(answer.equals(option2))
            button = mBinding.quizOption2;
        else if(answer.equals(option3))
            button = mBinding.quizOption3;
        else if(answer.equals(option4))
            button = mBinding.quizOption4;

        button.getBackground().setColorFilter(getResources().getColor(R.color.answer_true), PorterDuff.Mode.MULTIPLY);
        return button;
    }

    //Set Layout Question
    private void setLayout(){
        cleanAnswer();
        setButtonClick(true);
        currentQuestion = Tools.getRandomQuestion(mQuestions);
        mBinding.setQuestion(currentQuestion);
        String indexText = getString(R.string.quiz_index) + " " + String.valueOf(index);
        mBinding.quizIndex.setText(indexText);
    }

    //Open Statistics after Game Over
    private void openStatisticsActivity(){
        Intent intent = new Intent(this, StatisticsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(CORRECT_QUESTIONS,correct);
        startActivity(intent);
    }
}
