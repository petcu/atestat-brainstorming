package com.apps.petcu.brainstormingquiz.tools;

import com.apps.petcu.brainstormingquiz.model.Question;

import java.util.ArrayList;
import java.util.Random;

public class Tools {


    private static int getRandomNr(int range){
        return new Random().nextInt(range);
    }

    private static ArrayList<Integer> getAnswersOrder(){
        ArrayList<Integer> list = new ArrayList<>();
        while(list.size()<4){
            int nr = getRandomNr(4);
            if(!list.contains(nr))
                list.add(nr);
        }
        return list;
    }

    private static void reorderAnswers(Question question){
        ArrayList<Integer> list = getAnswersOrder();

        //Save answers in buffer
        ArrayList<String> buffer = new ArrayList<>();
        buffer.add(question.getOption1());
        buffer.add(question.getOption2());
        buffer.add(question.getOption3());
        buffer.add(question.getOption4());

        //Reorder Answers
        question.setOption1(buffer.get(list.get(0)));
        question.setOption2(buffer.get(list.get(1)));
        question.setOption3(buffer.get(list.get(2)));
        question.setOption4(buffer.get(list.get(3)));
    }

    public static Question getRandomQuestion(ArrayList<Question> mQuestions){
        Question question = mQuestions.get(getRandomNr(mQuestions.size()));
        mQuestions.remove(question);
        reorderAnswers(question);
        return question;
    }

}
