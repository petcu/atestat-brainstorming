package com.apps.petcu.brainstormingquiz.tools;

import android.content.res.Resources;

import com.apps.petcu.brainstormingquiz.R;

public class StringUtils {

    public static String setIndex(Integer index){
        String text = Resources.getSystem().getString(R.string.quiz_index);
        return text + " " + String.valueOf(index);
    }

}
