package com.apps.petcu.brainstormingquiz.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.petcu.brainstormingquiz.R;
import com.apps.petcu.brainstormingquiz.model.Category;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class DomainAdapter extends RecyclerView.Adapter<DomainAdapter.DomainViewHolder> {
    private Context mContext;
    private ArrayList<Category> mCategories;
    private OnAdapterItemClickListener mListener;

    public DomainAdapter(Context mContext, ArrayList<Category> mCategories) {
        this.mContext = mContext;
        this.mCategories = mCategories;
    }

    @NonNull
    @Override
    public DomainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.domain_item, parent, false);
        return new DomainViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DomainViewHolder holder, int position) {
        Category categoryCurrent = mCategories.get(position);
        holder.domain_name.setText(categoryCurrent.getName());
        Picasso.get()
                .load(categoryCurrent.getIconUrl())
                .placeholder(R.mipmap.ic_launcher)
                .fit()
                .centerInside()
                .into(holder.domain_image);
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    public void setOnDomainClickListener(OnAdapterItemClickListener listener) {
        mListener = listener;
    }

    public interface OnAdapterItemClickListener {
        void setDomainClick(CardView view, String category_name);
    }

    public class DomainViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView domain_image;
        private TextView domain_name;
        private CardView domain_card;

        public DomainViewHolder(@NonNull View itemView) {
            super(itemView);
            domain_card = itemView.findViewById(R.id.domain_card);
            domain_name = itemView.findViewById(R.id.domain_name);
            domain_image = itemView.findViewById(R.id.domain_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mListener != null){
                int position = getAdapterPosition();
                if(position != RecyclerView.NO_POSITION)
                    mListener.setDomainClick(domain_card, domain_name.getText().toString());
            }
        }
    }
}
