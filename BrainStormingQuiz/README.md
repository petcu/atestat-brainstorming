## BrainStormingQuiz
<p align="center">
  <img src="https://i.imgur.com/gXAAPH6.png" alt="BrainStormingQuiz"/>
</p>

### Download
https://github.com/PMS25/BrainStormingQuiz/releases

### Schreenshots
<p align="center">
  <img src="https://i.imgur.com/8hDJbz9.png" alt="screenshot_1"/>
  <img src="https://i.imgur.com/LVKhw8g.png" alt="screenshot_2"/>
  <img src="https://i.imgur.com/oi3DNHv.png" alt="screenshot_3"/>
  <img src="https://i.imgur.com/L7S5yDH.png" alt="screenshot_4"/>
</p>
